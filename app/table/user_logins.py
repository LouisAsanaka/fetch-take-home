from app.db import metadata

from sqlalchemy import Table, Column, Integer, String, DateTime
from sqlalchemy.sql import func


"""Table schema from the given DDL.
"""
user_logins = Table(
    'user_logins',
    metadata,
    Column('user_id', String(128)),
    Column('device_type', String(32)),
    Column('masked_ip', String(256)),
    Column('masked_device_id', String(256)),
    Column('locale', String(32)),
    Column('app_version', Integer),
    Column('create_date', DateTime(timezone=True), default=func.now())
)
