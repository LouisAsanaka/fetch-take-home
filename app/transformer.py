import os
import json
import logging
import hashlib
from typing import Optional, Dict


MASK_HASH_SALT: bytes = os.getenv('MASK_HASH_SALT', 'default').encode('utf-8')

logger = logging.getLogger(__name__)



class UserLoginTransformer:
    """Transformer for a UserLogin message.
    """

    @staticmethod
    def _extract_version(version_str: str) -> int:
        """Extracts the integer version number from a version string.

        Args:
            version_str: the version string to parse.
        Returns:
            an integer representing the major version.

        """
        return int(version_str.split('.')[0])

    @staticmethod
    def _mask(unmasked: str) -> str:
        """Masks the given string by SHA-256 hashing with a salt.

        Args:
            unmasked: the string to mask.
        Returns:
            a string representing the masked data (SHA-256 hash).

        """
        return hashlib.sha256(MASK_HASH_SALT + unmasked.encode('utf-8')).hexdigest()
    
    @staticmethod
    def transform(message) -> Optional[Dict]:
        """Transforms the UserLogin message by masking PII (IDs & IPs).

        Args:
            message: the UserLogin message to transform.
        Returns:
            a dict representing the masked data, or None if the message is
            invalid.

        """
        try:
            body: Dict = json.loads(message.body)
            user_id: str = body['user_id']
            app_version: str = UserLoginTransformer._extract_version(body['app_version'])
            device_type: str = body['device_type']
            ip: str = body['ip']
            masked_ip: str = UserLoginTransformer._mask(ip)
            locale: str = body['locale']
            device_id: str = body['device_id']
            masked_device_id: str = UserLoginTransformer._mask(device_id)
            return {'user_id': user_id, 'device_type': device_type, 
                'masked_ip': masked_ip, 'masked_device_id': masked_device_id, 
                'locale': locale, 'app_version': app_version}
        except KeyError as e:
            logger.warning('Message body contains missing keys')
            logger.warning(e)
        except json.JSONDecodeError as e:
            logger.warning('Message body is not valid JSON')
            logger.warning(e)
        return None
