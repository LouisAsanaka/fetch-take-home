import os
import logging
from typing import List

import boto3
from botocore.exceptions import ClientError


ENDPOINT_URL: str = os.getenv('ENDPOINT_URL', 'http://localhost:4566')
REGION_NAME: str = os.getenv('REGION_NAME', 'us-east-1')
ACCESS_KEY_ID: str = os.getenv('ACCESS_KEY_ID', 'test')
SECRET_ACCESS_KEY: str = os.getenv('SECRET_ACCESS_KEY', 'test')

logger = logging.getLogger(__name__)


class Consumer:

    def __init__(self, queue_name: str):
        """Initialize a SQS queue consumer for the given queue.

        Args:
            queue_name: the SQS queue to consume from.
        Raises:
            ClientError: if the queue doesn't exist.

        """
        self.sqs = boto3.resource('sqs', region_name=REGION_NAME, 
            endpoint_url=ENDPOINT_URL, aws_access_key_id=ACCESS_KEY_ID, 
            aws_secret_access_key=SECRET_ACCESS_KEY)
        # Attempt to retrieve the pre-existing queue
        try:
            self.queue = self.sqs.get_queue_by_name(QueueName=queue_name)
        except ClientError as error:
            logger.exception(f'Couldn\'t get queue named {queue_name}.')
            raise error
    
    def consume(self, max_messages: int, timeout: int) -> List:
        """Attempt to consume messages from the SQS queue.

        Args:
            max_messages: maximum number of messages to return.
            timeout: timeout in seconds, set to 0 for short-poll.
        Raises:
            ClientError: if the queue couldn't be consumed from.

        """
        try:
            messages = self.queue.receive_messages(
                MessageAttributeNames=['All'],
                MaxNumberOfMessages=max_messages,
                WaitTimeSeconds=timeout
            )
        except ClientError as error:
            logger.exception(f'Couldn\'t receive messages from queue: {self.queue}')
            raise error
        else:
            return messages
