import os
from typing import List, Dict

from sqlalchemy import create_engine
from sqlalchemy import MetaData


DB_USERNAME: str = os.getenv('DB_USERNAME', 'postgres')
DB_PASSWORD: str = os.getenv('DB_PASSWORD', 'postgres')
DB_HOST: str = os.getenv('DB_HOST', 'localhost')
DB_PORT: str = os.getenv('DB_PORT', '5432')
DB_NAME: str = os.getenv('DB_NAME', 'postgres')
CONNECTION_STRING: str = f'postgresql+psycopg2://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'

metadata = MetaData()


class DB:
    """Simple wrapper around the SQLAlchemy database connection.
    """

    def __init__(self):
        self.engine = create_engine(CONNECTION_STRING, echo=False, future=True)

    def __del__(self):
        self.engine.dispose()

    def insert(self, table, records: List[Dict]):
        """Inserts the given records into a table.

        Args:
            table: the table to insert the records into.
            records: the list of records to insert.

        """
        with self.engine.begin() as conn:
            conn.execute(table.insert(), records)
