from time import sleep
import logging
logging.basicConfig(level=logging.INFO)

from dotenv import load_dotenv
load_dotenv('.env')

from app.consumer import Consumer
from app.transformer import UserLoginTransformer
from app.db import DB
from app.table.user_logins import user_logins

MAX_MESSAGES: int = 10
TIMEOUT: int = 3
QUEUE_NAME: str = 'login-queue'

logger = logging.getLogger(__name__)


def main():
    """Main function for the application to perform ETL.
    """

    # Instantiate consumer for Extracting, and database for Loading
    consumer = Consumer(queue_name=QUEUE_NAME)
    db = DB()
    try:
        while True:
            # E - extract phase, consume from SQS queue
            messages = consumer.consume(max_messages=MAX_MESSAGES, timeout=TIMEOUT)
            
            # T - transform phase, mask PII
            records = []
            for message in messages:
                record = UserLoginTransformer.transform(message)
                if record:
                    records.append(record)
            
            # L - load phase, load masked data into database
            # Perform insertion if there are valid records
            if records:
                db.insert(user_logins, records)
                logger.info(f'Inserted {len(records)} records...')

            # Repeat with pause
            sleep(0.2)
    except KeyboardInterrupt:
        logger.info('Gracefully exiting...')
        return


if __name__ == '__main__':
    main()
