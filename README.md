# Fetch Rewards Data Engineering Take Home

### Setup
- Ensure [Docker](https://docs.docker.com/get-docker) is installed. Also install [psql](https://www.postgresql.org/download) if you would like to view the data.
- Clone the repository (`git clone https://gitlab.com/LouisAsanaka/fetch-take-home.git`).
- Enter the project directory (`cd fetch-take-home`).
- Build & launch the Docker Compose project (`docker compose up -d --build`).
- View the data in the database (`psql -d postgres -U postgres -p 5432 -h localhost -W` then `select * from user_logins;`).

### Architecture
- I opted for a small Python application using [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) to receive messages from the SQS queue and [SQLAlchemy](https://www.sqlalchemy.org) to write to the Postgres database.
- ETL (Extract, Transform, Load)
    - Extract: Consuming from the SQS queue with boto3
    - Transform: Transforming (masking PII) in pure Python
    - Load: Writing transformed data into Postgres with SQLAlchemy

### Questions
- How would you deploy this application in production?
    - I would deploy the application on a Kubernetes cluster as a microservice, and since I wrote a Dockerfile, it would be pretty simple to do so.
- What other components would you want to add to make this production ready?
    - Currently, secrets are loaded from environment variables, but for production using a secrets manager like Hashicorp Vault or Kubernetes Secrets (which is less secure).
    - Healthchecks would help monitor the status of the service in production.
    - Metrics monitoring with Prometheus would help ensure that the service is not overloaded.
- How can this application scale with a growing dataset?
    - Adding a buffer between the queue and the database would help lessen the load on the database if the database writes were the bottleneck.
    - Increasing the messages received per call would increase throughput as well.
- How can PII be recovered later on?
    - Since I opted for SHA-256 hashing with a salt, if the salt was compromised, an attacker could brute force the input PII. For example, for the IP, there are 2^32 ≈ 4 billion IPv4 addresses (less due to reserved ranges). An attacker with the salt could build a rainbow table by enumerating all possible IPs and performing a reverse lookup on the masked IPs. As for the masked device ID, which seems to be a UUID, there are 2^122 possibilities, which is much harder to brute force, and thus less of a problem. Regardless, hashing is not a perfect option for masking PII, but it is good enough and is deterministic, which allows analysts to compare same IPs without knowing the original IPs.
- What are the assumptions you made?
    - The `app_version` column takes the first integer from the version string in the message. The original body contains a semver-like string, but the given DDL has an integer as the column.
    - Hashing with a salt is a valid masking mechanism for the PIIs.
    - No primary key column is used in the database (as seen in the DDL).

### Progress Notes
- How to mask PII while keeping duplicates identical?
    - Encrypting with a key stored as a secret
        - Data is fully recoverable if the key is leaked
    - Plain hashing
        - Easily brute forcable depending on type of data stored
    - Hashing with a salt
        - Brute force possible if salt is leaked
    - Some sort of HMAC with a secret key
        - Similar problem as hashing with a salt
- How to communicate with database?
    - I have used SQLAlchemy a lot in the past, so it was what I opted for as it is powerful but also expressive enough for a small app.

### Next Steps
- Flesh out the DDL schema a bit more (`app_version` and lack of primary key)
- Load secrets through a more secure source than environment variables
- Add monitoring and metrics for the service
